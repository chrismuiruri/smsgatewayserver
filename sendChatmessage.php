<?php

if (isset($_GET["mobile_number"]) && isset($_GET["message"]) && isset($_GET['senderNumber'])) {
    $mobile_number = $_GET["mobile_number"];
    $message = $_GET["message"];
    $from = $_GET['senderNumber'];

    include_once './GCM.php';
    include_once './db_functions.php';

    $gcm = new GCM();
    $db = new DB_Functions();

    $regId = $db->getGcmID($mobile_number);

    $registration_ids = array($regId);
    $message = array("price" => $message, "kutoka" => $from);

    $result = $gcm->send_notification($registration_ids, $message);

    echo $result;
}
?>
