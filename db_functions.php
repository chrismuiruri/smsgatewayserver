<?php

class DB_Functions {

    private $db;

    // constructor
    function __construct() {
        include_once './db_connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->db->connect();
    }

    // destructor
    function __destruct() {
        
    }

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($username, $mobile_number, $registration_id) {
        // insert user into database
        $result = mysql_query("INSERT INTO users(username, mobile_number, registration_id, created_at) VALUES('$username', '$mobile_number', '$registration_id', NOW())");
        // check for successful store
        if ($result) {
            // get user details
            $id = mysql_insert_id(); // last inserted id
            $result = mysql_query("SELECT * FROM users WHERE id = $id") or die(mysql_error());
            // return user details
            if (mysql_num_rows($result) > 0) {
                return mysql_fetch_array($result);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get user by email and password
     */
    public function getUserByEmail($mobile_number) {
        $result = mysql_query("SELECT * FROM users WHERE mobile_number = '$mobile_number' LIMIT 1");
        return $result;
    }

    /**
     * Getting all users
     */
    public function getAllUsers() {
        $result = mysql_query("select * FROM users");
        return $result;
    }

    /**
     * Check user is existed or not
     */
    public function isUserExisted($mobile_number) {
        $result = mysql_query("SELECT email from users WHERE mobile_number = '$mobile_number'");
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            // user existed
            return true;
        } else {
            // user not existed
            return false;
        }
    }

    public function getGcmID($mobile_number) {
        $result = mysql_query("SELECT * from users WHERE mobile_number = '$mobile_number'");
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            $user_details = mysql_fetch_assoc($result);
            return $user_details['registration_id'] ;
        } else {
            // user not existed
            return "";
        }
    }


}

?>