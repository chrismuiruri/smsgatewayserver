<?php
// response json
$json = array();

/**
 * Registering a user device
 * Store registration id in users table
 */
if (isset($_GET["username"]) && isset($_GET["mobile_number"]) && isset($_GET["registration_id"])) {
    $username = $_GET["username"];
    $mobile_number = $_GET["mobile_number"];
    $registration_id = $_GET["registration_id"]; // GCM Registration ID
    // Store user details in db
    include_once './db_functions.php';
    include_once './GCM.php';

    $db = new DB_Functions();
    $gcm = new GCM();

    $res = $db->storeUser($username, $mobile_number, $registration_id);

    if ($res) {
	echo "success";
    } else {
	echo "failure";
    }
    
} else {
    // user details missing
    echo "missing";
}
?>